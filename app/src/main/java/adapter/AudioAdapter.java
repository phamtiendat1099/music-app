package adapter;


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.example.musicpro.R;

import java.util.ArrayList;
import java.util.List;

import model.Audio;

public class AudioAdapter extends BaseAdapter {

    private Context mContext;
    private List<Audio> lstAudio;
    private LayoutInflater layoutInflater;

    public AudioAdapter(Context applicationContext, ArrayList<Audio> lstAudio) {

        this.mContext = applicationContext;
        this.lstAudio = lstAudio;
        layoutInflater = LayoutInflater.from(mContext);
    }

    @Override
    public int getCount() {
        return lstAudio.size();
    }

    @Override
    public Object getItem(int position) {
        return lstAudio.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null) {
            convertView = layoutInflater.inflate(R.layout.custom_audio, null);
            holder = new ViewHolder();
            holder.audioName = (TextView) convertView.findViewById(R.id.audio_name);
            holder.audioAuthor = (TextView) convertView.findViewById(R.id.audio_author);
            holder.audioView = (TextView) convertView.findViewById(R.id. audio_view);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        Audio audio = lstAudio.get(position);
        holder.audioName.setText(audio.getAudioName());
        holder.audioAuthor.setText(audio.getAudioAuthor());
        holder.audioView.setText(String.valueOf(audio.getView()));

        return convertView;
    }

    static class ViewHolder {

        TextView audioName, audioAuthor, audioView;
    }

}
