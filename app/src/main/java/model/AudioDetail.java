package model;

public class AudioDetail {

    private Audio audioInfo;

    private AudioComment commentInfo;

    public Audio getAudioInfo() {
        return audioInfo;
    }

    public void setAudioInfo(Audio audioInfo) {
        this.audioInfo = audioInfo;
    }

    public AudioComment getCommentInfo() {
        return commentInfo;
    }

    public void setCommentInfo(AudioComment commentInfo) {
        this.commentInfo = commentInfo;
    }
}
