package common;

import android.util.Log;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class HttpRequestCommon {

    public static String sendGet(String url) throws IOException {
        HttpClient httpclient = new DefaultHttpClient();
        HttpGet httpget= new HttpGet(url);

        HttpResponse response = httpclient.execute(httpget);

        if(response.getStatusLine().getStatusCode()==200){
            String server_response = EntityUtils.toString(response.getEntity());
            return server_response;
        } else {
            Log.i("Server response", "Failed to get server response" );
        }
        return null;
    }


    public static String sendPost(String url, List<NameValuePair> nameValuePairs) throws IOException {

        HttpClient httpclient = new DefaultHttpClient();
        HttpPost httppost = new HttpPost(url);

//        List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);
//        nameValuePairs.add(new BasicNameValuePair("id", "12345"));
//        nameValuePairs.add(new BasicNameValuePair("stringdata", "Hi"));
        httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs,"UTF-8"));

        // Execute HTTP Post Request
        HttpResponse response = httpclient.execute(httppost);

        String server_response = EntityUtils.toString(response.getEntity());
        return server_response;

    }

}
