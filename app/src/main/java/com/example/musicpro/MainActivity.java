package com.example.musicpro;

import androidx.annotation.ColorLong;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;


import com.gauravk.audiovisualizer.visualizer.BarVisualizer;
import com.gauravk.audiovisualizer.visualizer.BlastVisualizer;
import com.gauravk.audiovisualizer.visualizer.BlobVisualizer;
import com.gauravk.audiovisualizer.visualizer.CircleLineVisualizer;
import com.gauravk.audiovisualizer.visualizer.HiFiVisualizer;
import com.gauravk.audiovisualizer.visualizer.WaveVisualizer;
import com.google.gson.Gson;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import common.HttpRequestCommon;
import model.AudioDetail;
import petrov.kristiyan.colorpicker.ColorPicker;

public class MainActivity extends AppCompatActivity {

    private Handler mHandler = new Handler();
    //Make sure you update Seekbar on UI thread

    private boolean playPause = true;
    private MediaPlayer mediaPlayer;
    private ProgressDialog progressDialog;
    private boolean initialStage = true;
    private String colorEffect = "000000";

    private BarVisualizer barVisualizer;
    private HiFiVisualizer hiFiVisualizer;
    private CircleLineVisualizer circleLineVisualizer;
    private BlobVisualizer blobVisualizer;
    private BlastVisualizer blastVisualizer;
    private WaveVisualizer waveVisualizer;
    private SeekBar seekBar;

    private TextView txtOption, src;
    private TextView txtSumTime, txtProcessTime;


    private ImageView btnAutoPlay, btnBack, btnPlay, btnNext, btnEffect, btnReload, colorPicker;

    private int count = 0;
    private int lengthCur = 0;
    private int pos;

    private boolean autoPlay = false;
    private boolean flagReload = false;

    private Button btnChange;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        init();

        event();


    }


    private void init(){

        ActivityCompat.requestPermissions(MainActivity.this,
                new String[]{Manifest.permission.RECORD_AUDIO},
                1);

        mediaPlayer = new MediaPlayer();
        mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
        progressDialog = new ProgressDialog(this);

        barVisualizer =  findViewById(R.id.bar);
        hiFiVisualizer = findViewById(R.id.hifi);
        circleLineVisualizer = findViewById(R.id.circle);
        blobVisualizer = findViewById(R.id.blob);
        blastVisualizer = findViewById(R.id.blast);
        waveVisualizer = findViewById(R.id.wave);


        btnNext = findViewById(R.id.btnNext);
        btnAutoPlay = findViewById(R.id.btnAutoPlay);
        btnPlay = findViewById(R.id.btnPlay);
        btnBack = findViewById(R.id.btnBack);
        btnEffect = findViewById(R.id.btnEffect);
        btnReload = findViewById(R.id.btnReload);
        colorPicker = findViewById(R.id.colorPicker);

        seekBar = findViewById(R.id.seekBar);

        txtOption = findViewById(R.id.option);
        src = findViewById(R.id.src);

        txtSumTime = findViewById(R.id.txtSumTime);
        txtProcessTime = findViewById(R.id.txtProcessTime);

    }

    private void event(){
        btnPlay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (playPause){

                    btnPlay.setImageResource(R.drawable.pause);
                    if (!mediaPlayer.isPlaying() && initialStage){
                        try {
                            playNewMusic();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                    else {
                        mediaPlayer.seekTo(lengthCur);
                        mediaPlayer.start();
                    }
                }
                else{
                    btnPlay.setImageResource(R.drawable.play);
                    mediaPlayer.pause();
                    lengthCur = mediaPlayer.getCurrentPosition();
                    //releaseAll();
                    //length=mediaPlayer.getCurrentPosition();
                }
                playPause=!playPause;
            }
        });


        btnEffect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                invisibleAll();
                count=(count+1)%6;
                if (count==0){
                    txtOption.setText("OPTION: BAR");
                    barVisualizer.setVisibility(View.VISIBLE);
                }
                else if (count==1){
                    txtOption.setText("OPTION: HIFI");
                    hiFiVisualizer.setVisibility(View.VISIBLE);
                }
                else if (count==2){
                    txtOption.setText("OPTION: CIRCLE");
                    circleLineVisualizer.setVisibility(View.VISIBLE);
                }
                else if (count==3){
                    txtOption.setText("OPTION: BLOB");
                    blobVisualizer.setVisibility(View.VISIBLE);
                }
                else if (count==4){
                    txtOption.setText("OPTION: BLAST");
                    blastVisualizer.setVisibility(View.VISIBLE);
                }
                else if (count==5){
                    txtOption.setText("OPTION: WAVE");
                    waveVisualizer.setVisibility(View.VISIBLE);
                }
                releaseAll();
                setAudioSession();
            }
        });

        btnReload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                flagReload = !flagReload;
                if (flagReload){
                    btnReload.setImageResource(R.drawable.b_reload);
                }else {
                    btnReload.setImageResource(R.drawable.reload);
                }
            }
        });

        mediaPlayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mp) {

                initialStage = false;
                seekBar.setMax(mediaPlayer.getDuration()/1000);
                txtSumTime.setText(coverTime((long)mediaPlayer.getDuration()/1000));
                setAudioSession();
                mediaPlayer.start();
            }
        });

        mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mp) {
                initialStage = true;
                releaseAll();

                if (flagReload || autoPlay){
                    mediaPlayer.reset();
                    try {
                        playNewMusic();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        });


        MainActivity.this.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if(!initialStage){
                    int mCurrentPosition = mediaPlayer.getCurrentPosition() / 1000;
                    seekBar.setProgress(mCurrentPosition);
                    txtProcessTime.setText(coverTime((long)mCurrentPosition));
                }
                mHandler.postDelayed(this, 1000);
            }
        });

        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                if(mediaPlayer != null && fromUser){
                    lengthCur = progress*1000;
                    mediaPlayer.seekTo(progress * 1000);
                    if (initialStage)
                        txtProcessTime.setText(coverTime((long)progress));
                    playPause = true;
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        btnAutoPlay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                autoPlay=!autoPlay;
                if (autoPlay){
                    btnAutoPlay.setImageResource(R.drawable.b_suft);
                }else
                    btnAutoPlay.setImageResource(R.drawable.suft);
            }
        });

        colorPicker.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ColorPicker colorPicker = new ColorPicker(MainActivity.this);
                colorPicker.show();
                colorPicker.setOnChooseColorListener(new ColorPicker.OnChooseColorListener() {
                    @Override
                    public void onChooseColor(int position,int color) {
                        barVisualizer.setColor(color);
                        hiFiVisualizer.setColor(color);
                        circleLineVisualizer.setColor(color);
                        blastVisualizer.setColor(color);
                        blobVisualizer.setColor(color);
                        waveVisualizer.setColor(color);
                        colorPicker.dismissDialog();
                    }

                    @Override
                    public void onCancel(){
                        // put code
                    }
                });
            }
        });

    }

    private void invisibleAll (){
        barVisualizer.setVisibility(View.INVISIBLE);
        hiFiVisualizer.setVisibility(View.INVISIBLE);
        circleLineVisualizer.setVisibility(View.INVISIBLE);
        blobVisualizer.setVisibility(View.INVISIBLE);
        blastVisualizer.setVisibility(View.INVISIBLE);
        waveVisualizer.setVisibility(View.INVISIBLE);
    }

    private void releaseAll(){
        barVisualizer.release();
        hiFiVisualizer.release();
        circleLineVisualizer.release();
        blobVisualizer.release();
        blastVisualizer.release();
        waveVisualizer.release();
    }

    private void setAudioSession(){

        int audioSessionId = mediaPlayer.getAudioSessionId();
        if (audioSessionId != -1) {
            if (count==0) {
                barVisualizer.setAudioSessionId(audioSessionId);
            }
            else if (count==1) {
                hiFiVisualizer.setAudioSessionId(audioSessionId);
            }
            else if (count==2) {
                circleLineVisualizer.setAudioSessionId(audioSessionId);
            }
            else if (count==3) {
                blobVisualizer.setAudioSessionId(audioSessionId);
            }
            else if (count==4) {
                blastVisualizer.setAudioSessionId(audioSessionId);
            }
            else if (count==5) {
                waveVisualizer.setAudioSessionId(audioSessionId);
            }
        }
    }

    private String coverTime(Long x){
        String st="";
        if (x/60 < 10) st+='0';
        st = st+x/60+":";
        if (x%60 < 10 ) st+='0';
        st = st+x%60;
        return st;
    }

    private void playNewMusic() throws IOException {
        Random rd = new Random();

        Intent intent = getIntent();
        Long audioId  = Long.valueOf(intent.getStringExtra("data"));

        List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
        nameValuePairs.add(new BasicNameValuePair("audioId", String.valueOf(audioId)));

        String response = HttpRequestCommon.sendPost("https://35.247.144.237:8080/StreamingAudio/audio_service/get_detail_audio",
                nameValuePairs);
        Gson gson = new Gson();
        AudioDetail audioDetail = gson.fromJson(response, AudioDetail.class);

        try {
            mediaPlayer.setDataSource("http://35.247.144.237:8080/StreamingAudio/streaming_service/stream_audio?audioId="+audioId);
//            src.setText("SRC: "+LINK_MP3[pos]);
        } catch (IOException e) {
            e.printStackTrace();
        }
        mediaPlayer.prepareAsync();
    }
}
