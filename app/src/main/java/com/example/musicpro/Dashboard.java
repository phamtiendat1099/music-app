package com.example.musicpro;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.os.StrictMode;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import adapter.AudioAdapter;
import common.HttpRequestCommon;
import model.Audio;

public class Dashboard extends AppCompatActivity {

    ListView listView;
    AudioAdapter audioAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);

        init();
        try {
            loadAllAudio();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    public void init(){
        listView = findViewById(R.id.listView);
    }

    public void loadAllAudio() throws IOException {

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();

        StrictMode.setThreadPolicy(policy);

        String res = HttpRequestCommon.sendGet("http://35.247.144.237:8080/StreamingAudio/audio_service/get_list_audio");

        Gson gson = new Gson();

        List<Audio> lstAudio;

        lstAudio = gson.fromJson(res, new TypeToken<List<Audio>>(){}.getType());

        audioAdapter = new AudioAdapter(getApplicationContext(), (ArrayList<Audio>) lstAudio);

        listView.setAdapter(audioAdapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Audio audio = (Audio) audioAdapter.getItem(position);

                Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                intent.putExtra("data", String.valueOf(audio.getAudioId()));
                startActivity(intent);
            }
        });


        
    }
}